import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:flutter_webrtc_sdk/flutter_webrtc_sdk.dart' as webrtc;
import 'package:player/common/controller.dart';
import 'package:player/common/player.dart';
import 'package:player/controllers/webrtc_controller.dart';
import 'package:player/controllers/whep_controller.dart';
import 'package:video_player/video_player.dart';

class WebRTCPlayer extends StatelessWidget {
  final webrtc.PlaylistRTCPlayerController controller;

  const WebRTCPlayer(this.controller, {super.key});

  @override
  Widget build(BuildContext context) {
    final player = RTCVideoView(controller.renderer);
    return player;
  }
}

class WhepPlayer extends StatelessWidget {
  final webrtc.WhepPlayerController controller;

  const WhepPlayer(this.controller, {super.key});

  @override
  Widget build(BuildContext context) {
    final player = RTCVideoView(controller.renderer);
    return player;
  }
}

class LitePlayer extends ILitePlayer {
  const LitePlayer({required IPlayerController controller, double aspectRatio = 16 / 9, Key? key})
      : super(controller: controller, aspectRatio: aspectRatio, key: key);

  @override
  _LitePlayerState createState() {
    return _LitePlayerState();
  }
}

class _LitePlayerState extends ILitePlayerState {
  @override
  Widget player() {
    if (controller is IWebRTCVideoPLayerController) {
      final player = WebRTCPlayer(controller.baseController);
      return AspectRatio(aspectRatio: controller.aspectRatio(), child: player);
    } else if (controller is IWhepVideoPlayerController) {
      final player = WhepPlayer(controller.baseController);
      return AspectRatio(aspectRatio: controller.aspectRatio(), child: player);
    }

    return AspectRatio(
        aspectRatio: controller.aspectRatio(), child: VideoPlayer(controller.baseController));
  }
}
