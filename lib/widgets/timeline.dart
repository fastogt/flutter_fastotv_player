import 'package:flutter/material.dart';
import 'package:player/common/states.dart';
import 'package:player/controllers/standart_controller.dart';
import 'package:video_player/video_player.dart';

class LitePlayerTimeline extends StatelessWidget {
  static const place =
      Padding(padding: EdgeInsets.only(top: 5.0), child: LinearProgressIndicator());
  final IStandartPlayerController controller;

  const LitePlayerTimeline(this.controller);

  @override
  Widget build(BuildContext context) {
    return PlayerStateBuilder(controller, builder: (BuildContext context, IPlayerState? state) {
      if (state is! InitIPlayerState) {
        return VideoProgressIndicator(controller.baseController!, allowScrubbing: true);
      }
      return place;
    });
  }
}
