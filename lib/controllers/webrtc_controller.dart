import 'dart:async';

import 'package:flutter_webrtc_sdk/flutter_webrtc_sdk.dart' as webrtc;
import 'package:player/common/controller.dart';
import 'package:player/common/states.dart';

bool isWebRTCProtocol(String url) {
  final Uri? uri = Uri.tryParse(url);
  if (uri == null) {
    return false;
  }
  return uri.isScheme('webrtcs') || uri.isScheme('webrtc');
}

abstract class IWebRTCVideoPLayerController
    extends IPlayerController<webrtc.PlaylistRTCPlayerController?> {
  final _controller = webrtc.PlaylistRTCPlayerController();
  late StreamSubscription<webrtc.IPlayerState> _sub;

  IWebRTCVideoPLayerController({Duration? initDuration, IPlayerLinkObserver? linkObserver})
      : super(linkObserver: linkObserver, initDuration: initDuration) {
    _sub = _controller.stream.listen((event) {
      if (event is webrtc.InitIPlayerState) {
        emit(InitIPlayerState());
      } else if (event is webrtc.ConnectingIPlayerState) {
        emit(ConnectingIPlayerState());
      } else if (event is webrtc.LoadingIPlayerState) {
        emit(LoadingIPlayerState());
      } else if (event is webrtc.ErrorIPlayerState) {
        emit(ErrorIPlayerState(event.error));
      } else if (event is webrtc.ReadyIPlayerState) {
        emit(ReadyIPlayerState(event.url));
      } else if (event is webrtc.PlayingIPlayerState) {
        emit(PlayingIPlayerState(event.url));
      }
    });
  }

  @override
  webrtc.PlaylistRTCPlayerController? get baseController {
    return _controller;
  }

  @override
  void setMute(bool mute) {
    if (_controller.mediaStream == null) {
      throw Future.error('Not implemented');
    }

    final audio = _controller.mediaStream?.getAudioTracks()[0];

    if (mute) {
      audio?.enabled = false;
    } else {
      audio?.enabled = true;
    }
  }

  @override
  double aspectRatio() {
    return 16 / 9;
  }

  @override
  bool isPlaying() {
    return _controller.stream is webrtc.PlayingIPlayerState;
  }

  @override
  Future<void> pause() {
    throw 'Not implemented';
  }

  @override
  Future<void> play() {
    throw 'Not implemented';
  }

  @override
  bool isEOS() {
    return false;
  }

  @override
  Duration position() {
    return const Duration();
  }

  @override
  Duration duration() {
    return const Duration();
  }

  @override
  Future<void> seekTo(Duration duration) {
    throw 'Not implemented';
  }

  @override
  Future<void> setPlaybackSpeed(double speed) {
    throw 'Not implemented';
  }

  @override
  Future<void> setVideoLink(String? url) {
    if (url == null) {
      return throw 'Invalid input';
    }

    final String sid = url.split('/').last;
    return _controller.connect(url, sid);
  }

  @override
  Future<void> setVolume(double volume) {
    throw 'Not implemented';
  }

  @override
  void setUrl(String url, Duration? duration) {
    setVideoLink(url);
  }

  @override
  void dispose() {
    _sub.cancel();
    _controller.dispose();
    super.dispose();
  }
}
