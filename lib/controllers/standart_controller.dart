import 'dart:async';

import 'package:player/common/controller.dart';
import 'package:player/player.dart';
import 'package:video_player/video_player.dart';

abstract class IStandartPlayerController extends IPlayerController<VideoPlayerController?> {
  final Map<String, String> httpHeaders;
  VideoPlayerController? _controller;
  List<Function> _listeners = [];

  IStandartPlayerController(
      {Duration? initDuration,
      IPlayerLinkObserver? linkObserver,
      this.httpHeaders = const <String, String>{}})
      : super(linkObserver: linkObserver, initDuration: initDuration);

  @override
  VideoPlayerController? get baseController {
    return _controller;
  }

  @override
  bool isPlaying() {
    if (_controller == null) {
      return false;
    }

    return _controller!.value.isPlaying;
  }

  @override
  bool isEOS() {
    if (_controller == null) {
      return false;
    }

    return position() == duration();
  }

  @override
  Duration position() {
    if (_controller == null) {
      return const Duration();
    }
    return _controller!.value.position;
  }

  @override
  Duration duration() {
    if (_controller == null) {
      return const Duration();
    }
    return _controller!.value.duration;
  }

  @override
  double aspectRatio() {
    if (_controller == null) {
      return 16 / 9;
    }

    return _controller!.value.aspectRatio;
  }

  @override
  Future<void> pause() {
    if (_controller == null) {
      throw 'Invalid state';
    }

    return _controller!.pause();
  }

  @override
  Future<void> play() {
    if (_controller == null) {
      throw 'Invalid state';
    }

    final res = _controller!.play();
    /*final pl = res.then((_) {
      if (_controller!.value.hasError) {
        throw _controller!.value.errorDescription!;
      }
    }, onError: (dynamic error) {
      throw error;
    });*/
    return res;
  }

  @override
  Future<void> seekTo(Duration duration) {
    if (_controller == null) {
      throw 'Invalid state';
    }

    return _controller!.seekTo(duration);
  }

  @override
  Future<void> setPlaybackSpeed(double speed) {
    if (_controller == null) {
      throw 'Invalid state';
    }

    if (speed <= 0) {
      throw 'Invalid speed';
    }

    return _controller!.setPlaybackSpeed(speed);
  }

  @override
  Future<void> setVolume(double volume) {
    if (_controller == null) {
      throw 'Invalid state';
    }
    return _controller!.setVolume(volume);
  }

  void addListener(Function() listener) {
    _listeners.add(listener);
  }

  void removeListener(Function() listener) {
    _listeners.remove(listener);
  }

  @override
  Future<void> setVideoLink(String? url) {
    if (url == null) {
      throw 'Invalid input';
    }

    final VideoPlayerController? old = _controller;
    _controller = VideoPlayerController.networkUrl(Uri.parse(url),
        httpHeaders: httpHeaders, videoPlayerOptions: VideoPlayerOptions(mixWithOthers: true));

    old?.pause();
    final resp = _controller!.initialize();
    _controller!.addListener(_onControllerStateChanged);
    final result = resp.then((_) {
      old?.removeListener(_onControllerStateChanged);
      old?.dispose();
    }, onError: (error) {
      old?.removeListener(_onControllerStateChanged);
      old?.dispose();
    });
    return result;
  }

  void _onControllerStateChanged() {
    final val = _controller!.value;
    if (val.isCompleted) {
      emit(EOSIPlayerState(_controller!.dataSource));
    }

    for (final list in _listeners) {
      list.call();
    }
  }

  @override
  void setMute(bool mute) {
    if (_controller == null) {
      throw Future.error('Not implemented');
    }

    if (mute) {
      _controller?.setVolume(0.0);
    } else {
      _controller?.setVolume(1.0);
    }
  }

  @override
  void dispose() {
    _controller?.removeListener(_onControllerStateChanged);
    _controller?.dispose();
    super.dispose();
  }
}
