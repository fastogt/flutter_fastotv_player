import 'dart:async';

import 'package:meta/meta.dart';
import 'package:player/common/states.dart';
import 'package:rxdart/subjects.dart';

abstract class IPlayerLinkObserver {
  @visibleForOverriding
  void onPlaying(IPlayerController cont, String url);
  @visibleForOverriding
  void onPlayingError(IPlayerController cont, String url);
  @visibleForOverriding
  void onEOS(IPlayerController cont, String url);
}

abstract class InterfacePlayerController {
  final BehaviorSubject<IPlayerState> _stateController = BehaviorSubject<IPlayerState>();

  Stream<IPlayerState> get stream => _stateController.stream;

  InterfacePlayerController();

  void setMute(bool mute);

  bool isPlaying();

  Duration position();

  Duration duration();

  double aspectRatio();

  Future<void> pause();

  Future<void> play();

  Future<void> seekTo(Duration duration);

  Future<void> setPlaybackSpeed(double speed);

  Future<void> setVolume(double volume);

  Future<void> setVideoLink(String? url);

  bool isEOS();

  void initLink();

  @protected
  void emit(IPlayerState state) {
    if (!_stateController.isClosed) {
      _stateController.add(state);
    }
  }

  @mustCallSuper
  void dispose() {
    emit(InitIPlayerState());
    _stateController.close();
  }
}

abstract class IPlayerController<T> extends InterfacePlayerController {
  final IPlayerLinkObserver? _linkObserver;

  final Duration? initDuration;

  IPlayerLinkObserver? get linkObserver {
    return _linkObserver;
  }

  String? get currentLink;

  T get baseController;

  IPlayerController({IPlayerLinkObserver? linkObserver, this.initDuration})
      : _linkObserver = linkObserver,
        super() {
    emit(InitIPlayerState());
  }

  @override
  void setMute(bool mute);
  @override
  bool isPlaying();

  @override
  Duration position();

  @override
  double aspectRatio();

  @override
  Future<void> pause();

  @override
  Future<void> play();

  @override
  Future<void> seekTo(Duration duration);

  @override
  Future<void> setPlaybackSpeed(double speed);

  @override
  Future<void> setVolume(double volume);

  @override
  Future<void> setVideoLink(String? url);

  Future<void> seekForward([Duration duration = const Duration(seconds: 5)]) {
    return seekTo(position() + duration);
  }

  Future<void> seekBackward([Duration duration = const Duration(seconds: 5)]) {
    return seekTo(position() - duration);
  }

  Future<void> seekToStart() {
    return seekTo(Duration.zero);
  }

  @override
  void emit(IPlayerState state) {
    if (state is ErrorIPlayerState) {
      _linkObserver?.onPlayingError(this, state.error);
    } else if (state is PlayingIPlayerState) {
      _linkObserver?.onPlaying.call(this, state.url);
    }  else if (state is EOSIPlayerState) {
      _linkObserver?.onEOS.call(this, state.url);
    }
    super.emit(state);
  }

  @override
  void initLink() {
    if (currentLink == null) {
      return;
    }
    setUrl(currentLink!, initDuration);
  }

  void setUrl(String url, Duration? duration) {
    emit(InitIPlayerState());
    final res = setVideoLink(url);
    emit(ConnectingIPlayerState());
    res.then((value) {
      final stab = url;
      emit(LoadingIPlayerState());
      if (duration != null) {
        seekTo(duration);
      }
      emit(ReadyIPlayerState(stab));
      final pl = play();
      pl.then((_) {
        emit(PlayingIPlayerState(stab));
      }, onError: (error) {
        emit(ErrorIPlayerState(error));
      });
    }, onError: (error) {
      emit(ErrorIPlayerState(error));
    });
  }
}
