import 'package:flutter/material.dart';
import 'package:player/common/controller.dart';

abstract class IPlayerState {}

class InitIPlayerState extends IPlayerState {}

class ConnectingIPlayerState extends IPlayerState {}

class LoadingIPlayerState extends IPlayerState {}

class ErrorIPlayerState extends IPlayerState {
  final dynamic error;
  ErrorIPlayerState(this.error);
}

class ReadyIPlayerState extends IPlayerState {
  final String url;
  ReadyIPlayerState(this.url);
}

class PlayingIPlayerState extends IPlayerState {
  final String url;
  PlayingIPlayerState(this.url);
}

class EOSIPlayerState extends IPlayerState {
  final String url;
  EOSIPlayerState(this.url);
}

// for controls
class PlayerStateBuilder extends StatelessWidget {
  final InterfacePlayerController controller;
  final Widget Function(BuildContext context, IPlayerState? state) builder;

  const PlayerStateBuilder(this.controller, {required this.builder});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<IPlayerState>(
        stream: controller.stream,
        initialData: InitIPlayerState(),
        builder: (context, snapshot) {
          return builder(context, snapshot.data);
        });
  }
}

// for players

typedef PlayerBuilder = Widget Function(BuildContext context, InterfacePlayerController controller);

class CustomPlayer extends StatelessWidget {
  static const _initFallback = Center(child: CircularProgressIndicator(color: Colors.green));
  static const _connetingFallback =
      Center(child: CircularProgressIndicator(color: Colors.lightGreen));
  static const _loadingFallback = Center(child: CircularProgressIndicator(color: Colors.yellow));
  static const _readyFallback = Center(child: CircularProgressIndicator(color: Colors.orange));
  static const _errorFallback = Center(child: CircularProgressIndicator(color: Colors.red));

  final InterfacePlayerController controller;
  final PlayerBuilder playerBuilder;
  final double aspectRatio;
  final Widget init, connecting, loading, ready, error;

  const CustomPlayer(
      {Key? key,
      required this.controller,
      required this.playerBuilder,
      this.aspectRatio = 16 / 9,
      this.init = _initFallback,
      this.connecting = _connetingFallback,
      this.loading = _loadingFallback,
      this.ready = _readyFallback,
      this.error = _errorFallback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
        aspectRatio: aspectRatio,
        child: Container(
            color: Colors.black,
            child: StreamBuilder<IPlayerState>(
                stream: controller.stream,
                builder: (ctx, snapshot) {
                  return _player(ctx, snapshot.data);
                })));
  }

  Widget _player(BuildContext context, IPlayerState? state) {
    if (state is PlayingIPlayerState) {
      return playerBuilder(context, controller);
    } else if (state is EOSIPlayerState) {
      return playerBuilder(context, controller);
    } else if (state is ReadyIPlayerState) {
      return ready;
    } else if (state is LoadingIPlayerState) {
      return loading;
    } else if (state is ConnectingIPlayerState) {
      return connecting;
    } else if (state is ErrorIPlayerState) {
      return error;
    }

    return init;
  }
}
