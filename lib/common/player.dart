import 'package:flutter/material.dart';
import 'package:player/common/controller.dart';
import 'package:player/common/states.dart';

abstract class ILitePlayer extends StatefulWidget {
  final IPlayerController controller;
  final double aspectRatio;

  const ILitePlayer({required this.controller, this.aspectRatio = 16 / 9, Key? key})
      : super(key: key);
}

abstract class ILitePlayerState extends State<ILitePlayer> {
  IPlayerController get controller => widget.controller;

  @override
  void initState() {
    super.initState();
    controller.initLink();
  }

  @override
  void didUpdateWidget(ILitePlayer oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.controller != widget.controller) {
      widget.controller.initLink();
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomPlayer(
        controller: controller,
        aspectRatio: widget.aspectRatio,
        playerBuilder: (context, controller) {
          return player();
        });
  }

  Widget player();
}
